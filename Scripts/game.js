
function menuSongs(){
document.getElementById("m1").style.display = "inline-block";
document.getElementById("m2").style.display = "none";
document.getElementsByClassName('song-menu')[0].style.display = "none";
document.getElementsByClassName('button-menu')[0].style.display = "inline-block";
}
function tancarMenus(){
document.getElementById("m1").style.display = "none";
document.getElementById("m2").style.display = "none";
document.getElementsByClassName('song-menu')[0].style.display = "inline-block";
document.getElementsByClassName('button-menu')[0].style.display = "inline-block";
}
function menuOptions(){
document.getElementById("m2").style.display = "inline-block";
document.getElementById("m1").style.display = "none";
document.getElementsByClassName('button-menu')[0].style.display = "none";
document.getElementsByClassName('song-menu')[0].style.display = "inline-block";
}

function changeOpacity(value){
	document.getElementsByClassName('oscuro')[0].style.opacity = value;
	saveOptions();
}

function changeSpeed(value) {
	globalVelocity = Number(value);
	saveOptions();
}

function changeOffset(value) {
	globalSoundOffset = Number(value);
	saveOptions();
}

function valueProgressBar(parametro){
	document.getElementById("progress-bar").style.width = parametro + "%";
	document.getElementById("progress-bar").setAttribute("aria-valuenow",parametro);
}
function ponerTextoAviso(mensaje){
	document.getElementById("mensaje_div").style.display = "initial";
	document.getElementById("mensaje").innerHTML = mensaje;
}
function ocultarAviso(){
	document.getElementById("mensaje_div").style.display = "none";
}
function hideUi(){
	document.getElementsByClassName('titulo')[0].style.display = "none";
	document.getElementsByClassName('titulo')[0].style.visibility = "visible";
	document.getElementsByClassName('subtitulo')[0].style.display = "none";
	document.getElementsByClassName('subtitulo')[0].style.visibility = "visible";
	document.getElementsByClassName('points')[0].style.display = "none";
	document.getElementsByClassName('points')[0].style.visibility = "visible";
	document.getElementsByClassName('combo')[0].style.display = "none";
	document.getElementsByClassName('combo')[0].style.visibility = "visible";
	document.getElementsByClassName('ranking')[0].style.display = "none";
	document.getElementsByClassName('ranking')[0].style.visibility = "visible";
	ocultarAviso();
}
function showUi(){
	document.getElementsByClassName('titulo')[0].style.display = "block";
	document.getElementsByClassName('subtitulo')[0].style.display = "block";
	document.getElementsByClassName('points')[0].style.display = "block";
	document.getElementsByClassName('combo')[0].style.display = "block";
	document.getElementsByClassName('ranking')[0].style.display = "initial";
}