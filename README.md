# jsmania

JSmania is a web based game made in JavaScript inspired in rhythm games such as Guitar Hero, StepMania or Osu!Mania.

This game is based on osu songs in order to work so feel free to download as many songs as you want to play them in JSmania.
There is a brief explanation on how to load songs in the wiki page.

Feel free to download this repository and modify it as you wish.