var finishedLoadingMap = false;
var firstHitObjectTime;
let timeToDown;

function mapHitObjects() {
    timeToDown = (canvas.height / (globalVelocity * globalOffet)) * 1000;
    finishedLoadingMap = false;
    firstHitObjectTime = songHitObjects[0].timeToHit;
    songHitObjects.forEach(ho => {
        let position = getPosition(ho.noteLine);
        if (ho.hitType == "1") {
            new Note(position, position == 500 || position == 700 ? "#12d4e6" : "#740166", ho.timeToHit);
            //new Note(position, position == 500 || position == 700 ? "#12d4e6" : "#740166", Number(ho.timeToHit) < timeToDown ? ho.timeTohit : Number(ho.timeToHit) - timeToDown);
        } else {
            new Slider(position, "#0075FF", ho.timeToHit, ho.sliderEndTime);
            //new Slider(position, "#0075FF", Number(ho.timeToHit) < timeToDown ? ho.timeToHit : Number(ho.timeToHit) - timeToDown, Number(ho.timeToHit) < timeToDown ? ho.sliderEndTime : Number(ho.sliderEndTime) - timeToDown);
        }
        
    });
}

function initializeSong() {
    initializeNotes();
    initializeSliders();
    setTimeout(playSong, timeToDown + globalSoundOffset);
    finishedLoadingMap = true;
}

function getPosition(noteLine){
    switch (noteLine) {
        case "64":
            return 500;
        case "192":
            return 600;
        case "320":
            return 700;
        case "448":
            return 800;
        default:
            return 500;
    }
}

