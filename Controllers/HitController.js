var songHitObjects = [];
var activeHitNotesArray = [];

class HitObject {
    constructor(noteLine, timeToHit, hitType, sliderEndTime) {
        this.noteLine = noteLine;
        this.timeToHit = timeToHit;
        this.hitType = hitType;
        this.sliderEndTime = sliderEndTime;
    }
}

class ActiveHitNote {
    constructor(noteLine, timeToHit, timeToEnd) {
        this.noteLine = noteLine;
        this.timeToHit = timeToHit;
        this.timeToEnd = timeToEnd;
    }
}

function CheckHit(time, noteLine) {
    time = time * 1000;
    let ho = activeHitNotesArray.find(hit => (hit.timeToHit <= time + 500 && hit.timeToHit >= time - 500) && hit.noteLine == noteLine);
    if (ho != undefined && ho != null) {
        if (ho.timeToEnd == 0) {
            CheckNote(time, ho);
            let index = activeHitNotesArray.indexOf(ho);
            activeHitNotesArray.splice(index, 1);
        } else {
            CheckSliderStart(time, ho, noteLine);
        }
    }
}

function CheckNote(time, ho) {
    if (ho.timeToHit > time + 400 || ho.timeToHit < time - 400) {
        console.log("miss");
        combo = 0;
    } else if (ho.timeToHit <= time + 200 && ho.timeToHit >= time - 200){
        console.log("great");
        scoreTotal += 300;
        combo++;
    } else if (ho.timeToHit <= time + 300 && ho.timeToHit >= time - 300){
        console.log("good");
        scoreTotal += 200;
        combo++;
    } else if (ho.timeToHit <= time + 400 && ho.timeToHit >= time - 400){
        console.log("bad");
        scoreTotal += 100;
        combo++;
    }
}

function CheckSliderStart(time, ho, noteLine) {
    if (ho.timeToHit > time + 400 || ho.timeToHit < time - 400) {
        console.log("slider start: miss");
        combo = 0;
    } else if (ho.timeToHit <= time + 200 && ho.timeToHit >= time - 200){
        console.log("slider start: great");
        scoreTotal += 150;
        combo++;
        setKeySliderStatus(noteLine, true);
    } else if (ho.timeToHit <= time + 300 && ho.timeToHit >= time - 300){
        console.log("slider start: good");
        scoreTotal += 100;
        combo++;
        setKeySliderStatus(noteLine, true);
    } else if (ho.timeToHit <= time + 400 && ho.timeToHit >= time - 400){
        console.log("slider start: bad");
        scoreTotal += 50;
        combo++;
        setKeySliderStatus(noteLine, true);
    }
}

function CheckSliderEnding(time, noteLine) {
    time = time * 1000;
    let ho = activeHitNotesArray.find(hit => (hit.timeToEnd <= time + 500 && hit.timeToEnd >= time - 500) && hit.noteLine == noteLine);
    if (ho != undefined && ho != null) {
        if (ho.timeToEnd > time + 400 || ho.timeToEnd < time - 400) {
            console.log("slider release: miss");
            combo = 0;
        } else if (ho.timeToEnd <= time + 200 && ho.timeToEnd >= time - 200){
            console.log("slider release: great");
            scoreTotal += 150;
            combo++;
        } else if (ho.timeToEnd <= time + 300 && ho.timeToEnd >= time - 300){
            console.log("slider release: good");
            scoreTotal += 100;
            combo++;
        } else if (ho.timeToEnd <= time + 400 && ho.timeToEnd >= time - 400){
            console.log("slider release: bad");
            scoreTotal += 50;
            combo++;
        }
        setKeySliderStatus(noteLine, false);
    } else if (getKeySliderStatus(noteLine) == true) {
        console.log("slider release: miss");
        combo = 0;
        setKeySliderStatus(noteLine, false);
    }
}

function setKeySliderStatus(noteLine, status){
    switch (noteLine) {
        case 500:
            dkeySliderPressed = status;
            break;
        case 600:
            fkeySliderPressed = status;
            break;
        case 700:
            jkeySliderPressed = status;
            break;
        case 800:
            kkeySliderPressed = status;
            break;
        default:
            break;
    }
}

function getKeySliderStatus(noteLine) {
    switch (noteLine) {
        case 500:
            return dkeySliderPressed;
        case 600:
            return fkeySliderPressed;
        case 700:
            return jkeySliderPressed;
        case 800:
            return kkeySliderPressed;
        default:
            return false;
    }
}