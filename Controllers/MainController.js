var canvas = document.getElementById('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
var context = canvas.getContext('2d');
var globalVelocity = 3;
var globalOffet = 100;
var globalSoundOffset = -500;
var started = false;
var scoreTotal = 0;
var combo = 0;
var currentBeatmap;
var timestamp;
var secondsPassed = 0;
var oldTimeStamp = 0;

function Start() {
    if (started == false) {
        initializeSong();
        started = true;
    } 
    // else {
    //     stopSong();
    //     started = false;
    // }
}

window.onload = function(){
    
    function calculateFPS() {
        timestamp = performance.now();
        secondsPassed = (timestamp - oldTimeStamp) / 1000;
        oldTimeStamp = timestamp;
        updateGame();
        requestAnimationFrame(calculateFPS);
    }
    
    loadGUI();
    loadBeatmaps();
    createKeys();
    calculateFPS();
}

function updateGame() {
    context.clearRect(0, 0, innerWidth, innerHeight);
    drawKeys();
    if (finishedLoadingMap == true && started == true) {
        drawSliders();
        drawNotes();
        updateGUI();
    }
}

window.onkeydown = function(event){
    if (event.repeat == false) {
        switch (event.keyCode) {
            case 32: 
                Start();
                break;
            case 68:
                playSound(dSound);
                dkey.isPressed = true;
                CheckHit(currentSong.currentTime, 500);
                break;
            case 70:
                playSound(fSound);
                fkey.isPressed = true;
                CheckHit(currentSong.currentTime, 600);
                break;
            case 74:
                playSound(jSound);
                jkey.isPressed = true;
                CheckHit(currentSong.currentTime, 700);
                break;
            case 75:
                playSound(kSound);
                kkey.isPressed = true;
                CheckHit(currentSong.currentTime, 800);
                break;
            case 186:
                globalVelocity--;
                break;
            case 187:
                globalVelocity++;
                break;
            default:
                break;
        }
    }
}

window.onkeyup = function(event){
    if (event.repeat == false) {
        switch (event.keyCode) {
            case 68: 
                dkey.isPressed = false;
                CheckSliderEnding(currentSong.currentTime, 500);
                break;
            case 70:
                fkey.isPressed = false;
                CheckSliderEnding(currentSong.currentTime, 600);
                break;
            case 74:
                jkey.isPressed = false;
                CheckSliderEnding(currentSong.currentTime, 700);
                break;
            case 75:
                kkey.isPressed = false;
                CheckSliderEnding(currentSong.currentTime, 800);
                break;
            default:
                break;
        }
    }
}