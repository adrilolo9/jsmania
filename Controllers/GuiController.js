var scoreElement = document.getElementsByClassName("points")[0];
var comboElement = document.getElementsByClassName("combo")[0];

function updateGUI(){
    scoreElement.innerText = scoreTotal;
    comboElement.innerText = "x" + combo;
    valueProgressBar(100 * currentSong.currentTime / currentSong.duration);
}

function loadGUI() {
    let selectedOption = localStorage.getItem("jsManiaOption");
	hideUi();
    showUi();
    loadOptions();
	switch(selectedOption){
		case "1":
			menuSongs();
		break;
		case "2":
			menuOptions();
		break;
	}
}

function loadSongGUI(bm){
    let songsWrapper = document.getElementsByClassName("music-name")[0];
    let songTitle = document.createElement("h6");
    songTitle.innerText = bm.songName.length > 15 ? bm.songName.substr(0, 15) + "..." : bm.songName;
    let songAuthor = document.createElement("p");
    songAuthor.innerText = bm.author;
    let songId = document.createElement("p");
    songId.innerText = bm.songId;
    songId.style.display = "none";
    songId.setAttribute("id", "songid");
    let songDiv = document.createElement("div");
    songDiv.appendChild(songTitle);
    songDiv.appendChild(songAuthor);
    songDiv.appendChild(songId);
    songDiv.setAttribute("id", "songs");
    songDiv.onclick = function () {
        selectSong(songDiv);
    };
    songsWrapper.appendChild(songDiv);
}

function selectSong(song) {
    let songId = song.lastChild;
    let bm = beatmapArray.find(bm => bm.songId == songId.innerText);
    if (bm != undefined && bm != null) {
        document.getElementsByClassName("titulo")[0].innerText = bm.songName;
        document.getElementsByClassName("subtitulo")[0].innerText = bm.author;
        currentBeatmap = bm;
        loadHitObjects(bm.fileContent);
        currentSong = new Audio(bm.songPath);
    }
}

function loadOptions() {
    let opacity = document.getElementById("opacity");
    let speed = document.getElementById("speed");
    let offset = document.getElementById("offset");
    
    let savedOpacity = localStorage.getItem("opacityOption");
    if (savedOpacity != undefined && savedOpacity != null) {
        opacity.value = savedOpacity;
        document.getElementsByClassName('oscuro')[0].style.opacity = savedOpacity;
    }
    let savedSpeed = localStorage.getItem("speedOption");
    if (savedSpeed != undefined && savedSpeed != null) {
        speed.value = savedSpeed;
        globalVelocity = Number(savedSpeed);
    }
    let savedOffset = localStorage.getItem("offsetOption");
    if (savedOffset != undefined && savedOffset != null) {
        offset.value = savedOffset;
        globalSoundOffset = Number(savedOffset);
    }
}

function saveOptions() {
    let opacity = document.getElementById("opacity");
    let speed = document.getElementById("speed");
    let offset = document.getElementById("offset");

    localStorage.setItem("opacityOption", opacity.value);
    localStorage.setItem("speedOption", speed.value);
    localStorage.setItem("offsetOption", offset.value);
}