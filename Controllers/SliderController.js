var activeSlidersArray = [];
var slidersArray = [];
var dkeySliderPressed = false;
var fkeySliderPressed = false;
var jkeySliderPressed = false;
var kkeySliderPressed = false;

class Slider {
    constructor (xPosition, color, timeToHit, timeToEnd) {
        this.xPosition = xPosition;
        this.color = color;
        this.timeToHit = timeToHit;
        this.timeToEnd = Number(timeToEnd) - Number(timeToHit);
        this.keepDrawing = true;
        this.sliderObject = new createSlider(xPosition, 0, color, timeToHit, this.timeToEnd, this.keepDrawing);
        this.activeHitNote = new ActiveHitNote(xPosition, timeToHit, timeToEnd);
        slidersArray.push(this);
        //this.animate(this.sliderObject);
        //this.sliderLengthControl(this.sliderObject);
    }

    timeoutToHit(slider){
        function addToActiveSliders() {
            activeSlidersArray.push(slider);
            activeHitNotesArray.push(slider.activeHitNote);
        }

        function stopSliderLength() {
            slider.sliderObject.setSliderSize();
        }

        setTimeout(addToActiveSliders, slider.sliderObject.timeToHit);
        setTimeout(stopSliderLength, Number(slider.timeToHit) + slider.timeToEnd);
    }

    animate(slider){
        if (slider.sliderObject.sliderPosition < window.innerHeight - 150) {
            slider.sliderObject.update();
        } else{
            let index = activeSlidersArray.indexOf(slider);
            let indexHit = activeHitNotesArray.indexOf(slider.activeHitNote);
            if (index > -1) {
                activeSlidersArray.splice(index, 1);
            }
            if (indexHit > -1) {
                activeHitNotesArray.splice(indexHit, 1);
                console.log("slider not touched, miss.");
            }
        }
    }
}

function createSlider(xPosition, yPosition, color, timeToHit, timeToEnd, keepDrawing){

    this.sliderPosition = yPosition;
    this.timeToHit = timeToHit;
    this.timeToEnd = timeToEnd;
    this.yHeight = 0;
    this.keepDrawing = keepDrawing;

    this.setSliderSize = function() {
        this.keepDrawing = false;
    }

    this.draw = function(){
        context.beginPath();
        context.rect(xPosition, yPosition, 100, this.yHeight)
        context.fillStyle = color;
        context.fill();
    }
  
    this.update = function(){
        if (yPosition <= window.innerHeight - 120) {
            yPosition += (globalVelocity * globalOffet * secondsPassed);
        } else {
            this.yHeight += (globalVelocity * globalOffet * secondsPassed);
        }

        if (this.keepDrawing == true) {
            this.yHeight -= (globalVelocity * globalOffet * secondsPassed);
        }      
      
        this.sliderPosition = yPosition + this.yHeight;
  
        this.draw();
    }
}

function initializeSliders() {
    slidersArray.forEach(slider => {
        slider.timeoutToHit(slider);
    });
}

function drawSliders() {
    activeSlidersArray.forEach(slider => {
        slider.animate(slider);
    });
}