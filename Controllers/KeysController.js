var dkey;
var fkey;
var jkey;
var kkey;

class Key {
    constructor (xPosition, color, keyCode) {
        this.isPressed = false;
        this.keyCode = keyCode;
        this.xPosition = xPosition;
        this.color = color;
        this.keyObject = new createKey(xPosition, color, this.keyCode);
    }
}

function createKey(xPosition, color, keyCode){

    this.draw = function(keyColor){
      context.beginPath();
      context.rect(xPosition, window.innerHeight-150, 100, 150)
      context.fillStyle = keyColor;
      context.fill();
    }
  
    this.update = function(){
      let isPressed = this.pressed();
      this.draw(isPressed == false ? color : "#8a005a");
    }

    this.pressed = function(){
        switch (keyCode) {
            case 68:
                return dkey.isPressed;
            case 70:
                return fkey.isPressed;
            case 74:
                return jkey.isPressed;
            case 75:
                return kkey.isPressed;
            default:
                return false;
        }
      }
}

function createKeys() {
    dkey = new Key(500, "white", 68);
    fkey = new Key(600, "gray", 70);
    jkey = new Key(700, "white", 74);
    kkey = new Key(800, "gray", 75);
}

function drawKeys() {
    dkey.keyObject.update();
    fkey.keyObject.update();
    jkey.keyObject.update();
    kkey.keyObject.update();
}