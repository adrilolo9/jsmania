var activeNotesArray = [];
var notesArray = [];

class Note {
    constructor (xPosition, color, timeToHit) {
        this.xPosition = xPosition;
        this.color = color;
        this.timeToHit = timeToHit;
        this.noteObject = new createNote(xPosition, 0, color, timeToHit);
        this.activeHitNote = new ActiveHitNote(xPosition, timeToHit, 0);
        notesArray.push(this);
    }

    timeoutToHit(note){

        function addToActiveNotes() {
            activeNotesArray.push(note);
            activeHitNotesArray.push(note.activeHitNote);
        }

        setTimeout(addToActiveNotes, note.noteObject.timeToHit);
    }

    animate(note){
        if (note.noteObject.notePosition < window.innerHeight - 150) {
            note.noteObject.update();
        } else{
            let index = activeNotesArray.indexOf(note);
            let indexHit = activeHitNotesArray.indexOf(note.activeHitNote);
            if (index > -1) {
                activeNotesArray.splice(index, 1);
            }
            if (indexHit > -1) {
                activeHitNotesArray.splice(indexHit, 1);
                console.log("not touched, miss.");
            }
        }
    }
}

function createNote(xPosition, yPosition, color, timeToHit){

    this.notePosition = yPosition;
    this.timeToHit = timeToHit;

    this.draw = function(){
      context.beginPath();
      context.rect(xPosition, yPosition, 100, 30)
      context.fillStyle = color;
      context.fill();
    }
  
    this.update = function(){
  
      yPosition += (globalVelocity * globalOffet * secondsPassed);

      this.notePosition = yPosition;
  
      this.draw();
    }
}

function initializeNotes() {
    notesArray.forEach(note => {
        note.timeoutToHit(note);
    });
}

function drawNotes() {
    activeNotesArray.forEach(note => {
        note.animate(note);
    });
}