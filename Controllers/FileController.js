var beatmapArray = [];
var pathArray = ["522660 Porter Robinson & Madeon - Shelter/Porter Robinson & Madeon - Shelter (Dellvangel) [Easy].osu",
                 //"522660 Porter Robinson & Madeon - Shelter/Porter Robinson & Madeon - Shelter (Dellvangel) [Normal].osu",
                 "288794 AKINO from bless4 - MIIRO/AKINO from bless4 - MIIRO ([ S a k u r a ]) [Normal].osu",
                 //"288794 AKINO from bless4 - MIIRO/AKINO from bless4 - MIIRO ([ S a k u r a ]) [Hard].osu",
                 "476691 DJ OKAWARI - Flower Dance/DJ OKAWARI - Flower Dance (Narcissu) [CS' Normal].osu",
                 //"476691 DJ OKAWARI - Flower Dance/DJ OKAWARI - Flower Dance (Narcissu) [CS' Hard].osu",
                 "440508 The Quick Brown Fox - Big Money/The Quick Brown Fox - Big Money (Mage) [Penny [EZ]].osu",
                 //"440508 The Quick Brown Fox - Big Money/The Quick Brown Fox - Big Money (Mage) [Nickel [NM]].osu",
                 "575053 Camellia - Exit This Earth's Atomosphere/Camellia - Exit This Earth's Atomosphere (Protastic101) [340.29 ms].osu"];

class Beatmap {
    constructor (songId, filePath, fileContent, songName, songPath, author, version) {
        this.songId = songId;
        this.filePath = filePath;
        this.fileContent = fileContent;
        this.songName = songName;
        this.songPath = songPath;
        this.author = author;
        this.version = version;
    }
}

function loadBeatmaps() {
    let countId = 0;
    pathArray.forEach(path => {
        let _path = "./../Beatmaps/" + path;
        fetch(_path)
        .then(response => response.text())
        .then(text => createBeatmap(countId++, _path, text))
    });   
}

function createBeatmap(songId, filePath, fileContent){
    let audioFileNameIndex = fileContent.indexOf("AudioFilename:") + 14;

    let folderPath = filePath.substr(0, filePath.lastIndexOf("/") + 1);
    let songName = fileContent.substring(fileContent.lastIndexOf("Title:") + 6, fileContent.indexOf("\n", fileContent.lastIndexOf("Title:") + 6)).trim();
    let songVer = fileContent.substring(fileContent.lastIndexOf("Version:") + 8, fileContent.indexOf("\n", fileContent.lastIndexOf("Version:") + 8)).trim();
    let songPath = folderPath + fileContent.substring(audioFileNameIndex, fileContent.indexOf("\n", audioFileNameIndex)).trim();
    let songAuthor = fileContent.substring(fileContent.lastIndexOf("Artist:") + 7, fileContent.indexOf("\n", fileContent.lastIndexOf("Artist:") + 7)).trim();

    let beatmap = new Beatmap(songId, filePath, fileContent, songName, songPath, songAuthor, songVer);

    loadSongGUI(beatmap);

    beatmapArray.push(beatmap);
}

function loadHitObjects(fileContent){
    let hitObjects = fileContent.substring(fileContent.lastIndexOf("[HitObjects]") + 12, fileContent.length).trim().split("\n");
    
    hitObjects.forEach(ho => {
        let hoParams = ho.split(",");
        let noteLine = hoParams[0];
        let timeToHit = hoParams[2];
        let hitType = hoParams[3];
        let sliderEndTime = ho.match(/:/g).length == 5 ? hoParams[hoParams.length - 1].substr(0, hoParams[hoParams.length - 1].indexOf(":")) : "0";

        let hitObject = new HitObject(noteLine, timeToHit, hitType, sliderEndTime);

        songHitObjects.push(hitObject);
    });

    mapHitObjects();
}